﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using DBcon;


namespace SafeSynTools
{
    public class SqlDbHelper
    {
        public SqlDbHelper()
        {
        }
        /// <summary>
        /// 从数据库中拿数据
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static DataTable GetQuery(string sql, string connectString)
        {
         
            SqlConnection conn = new SqlConnection(connectString);
            DataSet ds1 = new DataSet();
            try
            {
                conn.Open();                
                SqlDataAdapter sda = new SqlDataAdapter(sql, conn);
                sda.Fill(ds1);
            }
            catch(Exception ex)
            {
                
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Dispose();
                    conn.Close();
                }
            }
            return ds1.Tables[0];
        }
        /// <summary>
        /// 从数据库中拿数据
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static DataTable GetQuery(string sql, SqlParameter[] cmdParms, string connectString)
        {
            using (SqlConnection conn = new SqlConnection(connectString))
            {
                DataSet ds1 = new DataSet();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    PrepareCommand(cmd, conn, null, sql, cmdParms);
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds1);
                        cmd.Parameters.Clear();
                    }

                }
                catch (Exception ex)
                {

                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                    {
                        conn.Dispose();
                        conn.Close();
                    }
                }
                if (ds1.Tables.Count > 0)
                {
                    return ds1.Tables[0];
                }
                else
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// 用来判断数据是否存在
        /// </summary>
        /// <param name="strsql"></param>
        /// <returns></returns>
        public static int GetQueryCount(string strsql, string connectString)
       {
            SqlConnection conn = new SqlConnection(connectString);
            DataSet ds1 = new DataSet();
            try
            {
                conn.Open();
                SqlDataAdapter sda = new SqlDataAdapter(strsql, conn);
                sda.Fill(ds1);
            }
            catch 
            {
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Dispose();
                    conn.Close();
                }
            }
            return ds1.Tables[0].Rows.Count;
       }
        /// <summary>
       /// 执行存储过程
       /// </summary>
       /// <param name="dt"></param>
       /// <param name="maxlockid"></param>
       /// <returns></returns>
        public static int RunProCess(string proName, string connectString)
        {
           SqlConnection conn = new SqlConnection(connectString);
           int resulte = 1;
           try
           {
               //dt.Columns.Remove("lockid");
               conn.Open();
               SqlCommand cmd = new SqlCommand();
               cmd.Connection = conn;
               cmd.CommandText = proName;
               cmd.CommandType = CommandType.StoredProcedure;
               cmd.ExecuteNonQuery();
           }
           catch (Exception ex)
           {
               resulte = 0;
           }
           finally
           {
               if (conn.State == ConnectionState.Open)
               {
                   conn.Dispose();
                   conn.Close();
               }
           }
           return resulte;
        }
        public static int RunInsert(DataTable dt,string maxlockid,string ProName,string connectString)
        {
           SqlConnection conn = new SqlConnection(connectString);
           int resulte=0;
           try
           {
               //dt.Columns.Remove("lockid");
               conn.Open();
               SqlCommand cmd = new SqlCommand();
               cmd.Connection = conn;
               cmd.CommandText = ProName;
               cmd.CommandTimeout = 500000;
               cmd.CommandType = CommandType.StoredProcedure;
               cmd.Parameters.Add("@userInData", SqlDbType.Structured);
               cmd.Parameters.Add("@mxlockid", SqlDbType.VarChar);
               cmd.Parameters.Add("@Result", SqlDbType.Int);
               cmd.Parameters[0].Value = dt;
               cmd.Parameters[1].Value = maxlockid;
               cmd.Parameters["@Result"].Direction = ParameterDirection.ReturnValue;               
               cmd.ExecuteNonQuery();
               resulte=(int)cmd.Parameters["@Result"].Value;
           }
           catch (Exception ex)
           {
              // throw ex;
           }
           finally
           {
               if (conn.State == ConnectionState.Open)
               {
                   conn.Dispose();
                   conn.Close();
               }               
           }
          return  resulte;
        }
        /// <summary>
       /// 对数据库修改或插入的方法
       /// </summary>
       /// <param name="sql"></param>
       /// <returns></returns>
        public static void ExecuteNonQuery(string sql, string connectString)
        {
          //  localconnnStr = "Data Source=13.75.93.215;uid=report2;pwd=123456@qq.com;database=dafacloud;Connect Timeout=90000;";
            SqlConnection conn = new SqlConnection(connectString);
           try
           {
               conn.Open();
               SqlCommand loclsqlcmd = new SqlCommand(sql, conn);
               loclsqlcmd.ExecuteNonQuery();
           }
           catch  (Exception e)
           {
               
           }
           finally
           {
               if (conn.State == ConnectionState.Open)
               {
                   conn.Dispose();
                   conn.Close();
               }
           }
        }
        private static void PrepareCommand(SqlCommand cmd, SqlConnection conn, SqlTransaction trans, string cmdText, SqlParameter[] cmdParms)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            cmd.Connection = conn;
            cmd.CommandText = cmdText;
            if (trans != null)
                cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;//cmdType;
            if (cmdParms != null)
            {
                foreach (SqlParameter parm in cmdParms)
                    cmd.Parameters.Add(parm);
            }
        }
    }
}
